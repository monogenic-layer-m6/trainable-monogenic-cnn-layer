 A Bio-inspired Quaternion Local Phase CNN Layer with Contrast Invariance and Linear Sensitivity to Rotation Angles

## Abstract

Deep learning models have been particularly successful with image recognition using Convolutional Neural Networks (CNN). However, the learning of a contrast invariance and rotation equivariance response may fail even with very deep CNNs or by large data augmentations in training.

We were inspired by the V1 visual features of the mammalian visual system.  To emulate as much as possible the early visual system and add more equivariant capacities to the CNN, we present a  quaternion local phase convolutional neural network layer encoding two local phases. 


## Requirements

- Python (>=3.7)
- keras (>=2.2.4)
- tensorflow (2.0)
- tensordatasets (1.?)
- opencv (>=4.1.2)
- pandas (>=0.25.3)
- scikit-image (>=0.15.0)
- matplotlib (>=3.1.2)

